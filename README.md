# EasyTravel on Kubernetes!

This what we have done so EasyTravel is working:

1. Edit the CLUSTER_IP in `loadgen-config.yaml` to your cluster IP.
 - can be retireved by running `kubetl get svc`

## Start EasyTravel


 1. in Services: `kubectl apply -f .`
 2. in ConfigMaps: `kubectl apply -f .` 
 3. in Deployments: `kubectl apply -f .`


Access (when running on local machine) under  [http://127.0.0.1:30088/](http://127.0.0.1:30088/)